<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Models\User;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        if (Auth::attempt([
            'email' => $request->email,
            'password' => $request->password,
        ])) {
            $user = User::where('email', $request->email)->first();
            if ($user->is_admin()) {
                return redirect()->route('home');
            }
            return redirect()->route('welcome');
        }
        return redirect()->back();
    }

}
