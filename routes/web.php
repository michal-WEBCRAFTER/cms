<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

  Auth::routes();

//  Route::get('/home', 'HomeController@index')->name('welcome');

//  Auth::routes();

  Route::get('/home', 'HomeController@index')->name('home');



Route::post('/login',[
    'uses' => 'LoginController@login',
    'as' => 'login'
]);
Route::group(['middleware' => 'auth'], function(){
    Auth::routes();
    Route::get('/home', function ()
    {
        return view('welcome');
    })->name('welcome');
    Route::get('/admin', function () {
        return view('home');
    })->name('home');

});
